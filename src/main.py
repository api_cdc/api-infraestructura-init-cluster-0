'''
Created on Mar 26, 2018

@author: e
'''

class MicroservicioPropiedades:
    nombre = None
    path = None
    imagenDockerSrc = None
    targetGroupArn = None
    memoryReservation = None
    containerPort = None
    
    def __init__(self, nombre, path, imagenDockerSrc, targetGroupArn, memoryReservation,
                 containerPort):
        self.nombre = nombre
        self.path = path
        self.imagenDockerSrc = imagenDockerSrc
        self.targetGroupArn = targetGroupArn
        
        self.memoryReservation = memoryReservation
        self.containerPort = containerPort
    
    def getNombreArchivoTaskDefinition(self):
        return 'script.ecs.dev.%s.1.task-definition.json' % self.nombre
    
    def getNombreArchivoServiceDefinitionTemplate(self):
        return 'script.ecs.dev.%s.2.service-definition-template.json' % self.nombre

    def getNombreArchivoServiceDefinition(self):
        return 'script.ecs.dev.%s.3.service-definition.json' % self.nombre

class FileContentsFactory:
    tareaTemplate = None
    servicioTemplate = None
    crearTareaCmdTemplate = None 
    crearServicioCmdTemplate = None
    crearTGCmdTemplate = None 
    
    awsClusterName = None
    awsProfile = None
    awsVpcId = None
    awsListenerArn = None
    
    def __init__(self, awsClusterName, awsProfile, awsVpcId, awsListenerArn):
        self.awsClusterName = awsClusterName
        self.awsProfile = awsProfile
        self.awsVpcId = awsVpcId
        self.awsListenerArn = awsListenerArn
        
        self.crearTareaCmdTemplate = 'aws ecs register-task-definition --profile $AWS_PROFILE --cli-input-json file://%(awsTaskDefinition)s\n'
        self.crearServicioCmdTemplate = 'aws ecs create-service --profile $AWS_PROFILE --cli-input-json file://%(awsServiceDefinition)s\n'
        self.crearTGCmdTemplate = """# ----------- %(microservicioNombre)s -----------
echo '  --> Creando %(microservicioNombre)s'
export TG_ARN=`aws elbv2 create-target-group --profile $AWS_PROFILE --name %(microservicioNombre)s-tg --protocol HTTP \\
--port 80 --target-type instance --vpc-id $AWS_VPCID | jq -r .TargetGroups[0].TargetGroupArn`
export RULE_ARN=`aws elbv2 create-rule --profile $AWS_PROFILE --listener-arn $LISTENER_ARN \\
--priority %(prioridad)d --conditions Field=path-pattern,Values='%(microservicioPath)s/*' \\
--actions Type=forward,TargetGroupArn=$TG_ARN | jq -r .Rules[0].RuleArn`

# Actualizar script de eliminacion de reglas y target groups
echo '# ----------- %(microservicioNombre)s -----------' >> 0.delete-rules-y-tgs.sh
echo aws elbv2 delete-rule --profile \$AWS_PROFILE --rule-arn $RULE_ARN >> 0.delete-rules-y-tgs.sh
echo aws elbv2 delete-target-group --profile \$AWS_PROFILE  --target-group-arn $TG_ARN >> 0.delete-rules-y-tgs.sh
echo '' >> 0.delete-rules-y-tgs.sh

# Actualizar configuracion JSON del servicio ECS
template_json_service_config=$(<./%(nombreArchivoServiceDefinitionTemplate)s)
echo "${template_json_service_config//TARGET_GROUP_ARN/$TG_ARN}" > %(nombreArchivoServiceDefinition)s
 

"""
        
        self.tareaTemplate = """{
  "family": "%(microservicioNombre)s-task",
  "containerDefinitions": [
    {
      "name": "%(microservicioNombre)s",
      "image": "%(imagenDockerSrc)s",
      "cpu": 200,
      "memoryReservation": %(memoryReservation)d,
      "portMappings": [
        {
          "containerPort": %(containerPort)d,
          "protocol": "tcp"
        }
      ],
      "essential": true
    }
  ]
}"""

        self.servicioTemplate = """{
    "cluster": "%(clusterName)s",
    "serviceName": "%(microservicioNombre)s-service",
    "taskDefinition": "%(microservicioNombre)s-task",
    "loadBalancers": [
        {
            "targetGroupArn": "%(microservicioTargetGroup)s",
            "containerName": "%(microservicioNombre)s",
            "containerPort": %(containerPort)d
        }
    ],
    "desiredCount": 1,
    "role": "ecsServiceRole"
}"""

    def getContenidoArchivoTareas(self, microservicioProps):
        params = {'microservicioNombre': microservicioProps.nombre, 
                  'imagenDockerSrc': microservicioProps.imagenDockerSrc,
                  'memoryReservation': microservicioProps.memoryReservation,
                  'containerPort': microservicioProps.containerPort}
        
        return self.tareaTemplate % params
 
    def getContenidoArchivoServicio(self, microservicioProps):
        params = {'clusterName': self.awsClusterName, 'microservicioNombre': microservicioProps.nombre, 
                  'microservicioTargetGroup': microservicioProps.targetGroupArn,
                  'containerPort': microservicioProps.containerPort}
        
        return self.servicioTemplate % params
    
    
    def getContenidoCreacionTareasYServicios(self, microserviciosLst):
        result = []
        
        result.append('#!/bin/bash\n\n')
        result.append('export AWS_PROFILE=%s\n\n' % self.awsProfile)
        
        for ms in microserviciosLst:
            props = {'awsTaskDefinition': ms.getNombreArchivoTaskDefinition(), 
                     'awsServiceDefinition': ms.getNombreArchivoServiceDefinition()}
            result.append('# ----------- %s -----------\n' % ms.nombre)
            result.append(self.crearTareaCmdTemplate % props)
            result.append(self.crearServicioCmdTemplate % props)
            result.append('\n')
            
            
        return result 

    def getContenidoCreacionTargetGroups(self, microserviciosLst):
        result = []
        
        encabezado = """#!/bin/bash

export LISTENER_ARN=%(awsListenerArn)s
export AWS_PROFILE=%(awsProfile)s
export AWS_VPCID=%(awsVpcId)s

if [ -f 0.delete-rules-y-tgs.sh ]
  then
    chmod 755 0.delete-rules-y-tgs.sh
fi

#exit 1 

if [ -x 0.delete-rules-y-tgs.sh ]
  then
    echo "Eliminando 'rules' y 'target goups' existentes" 
    ./0.delete-rules-y-tgs.sh
fi

echo "#!/bin/bash\n" > 0.delete-rules-y-tgs.sh
echo "export AWS_PROFILE=%(awsProfile)s\n" >> 0.delete-rules-y-tgs.sh

""" % {'awsListenerArn': self.awsListenerArn, 'awsProfile': self.awsProfile, 'awsVpcId': self.awsVpcId}
        
        result.append(encabezado)
        
        prioridad = 100
        for ms in microserviciosLst:
            props = {'microservicioNombre': ms.nombre, 'microservicioPath': ms.path, 'prioridad': prioridad,
                     'nombreArchivoServiceDefinitionTemplate': ms.getNombreArchivoServiceDefinitionTemplate(),
                     'nombreArchivoServiceDefinition' : ms.getNombreArchivoServiceDefinition()}
            prioridad += 1
            
            result.append(self.crearTGCmdTemplate % props)
            result.append('\n')
            
            
        return result 

#-------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------

def getMicroserviciosPropsLstSH():
    result = []
    
    result.append(MicroservicioPropiedades('significado-vida', '', 'tongueroo/sinatra:latest', 
                                           'TARGET_GROUP_ARN', 128, 4567))
    
    return result

    
def getMicroserviciosPropsLstCCAPIGlobal():
    result = []
    
    result.append(MicroservicioPropiedades('api-global-reporte-credito', '/mx/v1/reportecredito', '10.11.5.155:2005/api-global/api-global-reporte-credito', 
                                           'TARGET_GROUP_ARN', 1024, 8080))
    
    
    return result

def getMicroserviciosPropsLstCC():
    result = []
    
    result.append(MicroservicioPropiedades('api-private-match', '/mx/v1/match', '10.11.5.155:2005/api/api-private-match', 
                                           'TARGET_GROUP_ARN', 512, 8080))
    
    result.append(MicroservicioPropiedades('api-private-match-folio', '/mx/v1/matchfolio', '10.11.5.155:2005/api/api-private-match-folio', 
                                           'TARGET_GROUP_ARN', 512, 8080))
    
    result.append(MicroservicioPropiedades('api-private-empleo', '/mx/v1/empleos', '10.11.5.155:2005/api/api-private-empleo', 
                                           'TARGET_GROUP_ARN', 512, 8080))
    
    return result


def generarArchivosDefinicion(microservicioProps, ff):
    fhft = open('scripts/%s' % microservicioProps.getNombreArchivoTaskDefinition(), 'w')
    fhfs = open('scripts/%s' % microservicioProps.getNombreArchivoServiceDefinitionTemplate(), 'w')
    
    fhft.write(ff.getContenidoArchivoTareas(microservicioProps))
    fhfs.write(ff.getContenidoArchivoServicio(microservicioProps))
    
    fhft.close()
    fhfs.close()


def generarScriptCreacionTareasYServicios(microserviciosLst, ff):
    contenido = ff.getContenidoCreacionTareasYServicios(microserviciosLst)
    
    fhSE = open('scripts/1.crear-tareas-y-servicios.sh', 'w')
    fhSE.writelines(contenido)
    fhSE.close()


def generarScriptCreacionTargetGroups(microserviciosLst, ff):
    contenido = ff.getContenidoCreacionTargetGroups(microserviciosLst)
    
    fhSE = open('scripts/0.crear-target-groups.sh', 'w')
    fhSE.writelines(contenido)
    fhSE.close()

    
def main():
    LISTENER_ARN_SH = 'arn:aws:elasticloadbalancing:us-west-1:456950441138:listener/app/alb-ecs-poc-1/4e8a0fbf2cf85c2f/efd24a0d6bffd3ff'
    VPCID_SH = 'vpc-9d78fefa'
    PROFILE_SH = 'sh-mmendez'
    CLUSTER_SH = 'api-digital-dev0'
    microserviciosLstSH = getMicroserviciosPropsLstSH()

    LISTENER_ARN_CC = 'arn:aws:elasticloadbalancing:us-west-1:912206688235:listener/app/LB-API-DEV-01/6c6b27912dc8a0ea/81fffc85a64b26ed'
    VPCID_CC = 'vpc-0a18026f'
    PROFILE_CC = 'us-west-1'
    CLUSTER_CC = 'C-API-DEV'
    microserviciosLstCC = getMicroserviciosPropsLstCCAPIGlobal()
    
    ffSH = FileContentsFactory(CLUSTER_SH, PROFILE_SH, VPCID_SH, LISTENER_ARN_SH)
    ffCC =  FileContentsFactory(CLUSTER_CC, PROFILE_CC, VPCID_CC, LISTENER_ARN_CC)
    
    if (False):
        ffActual = ffSH
        microserviciosLst = microserviciosLstSH
    else:
        ffActual = ffCC
        microserviciosLst = microserviciosLstCC
    
    for ms in microserviciosLst:
        generarArchivosDefinicion(ms, ffActual)
        
    generarScriptCreacionTareasYServicios(microserviciosLst, ffActual)
    generarScriptCreacionTargetGroups(microserviciosLst, ffActual)
    
    

main()

